﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour
{
    [SerializeField] RectTransform content;
    [SerializeField] RectTransform rtItem;
    [SerializeField] RectTransform rtView;

    public Transform GetTransformContent() {
        return content?.transform;
    }

    public RectTransform GetRectTransformItem() {
        return rtItem;
    }

    public RectTransform GetRectTransformView()
    {
        return rtView;
    }

    public void Attach(RectTransform rtDirectorView)
    {
        rtDirectorView.SetParent(rtItem, false);
    }
}