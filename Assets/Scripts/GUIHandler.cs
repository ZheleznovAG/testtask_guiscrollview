﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GUIHandler : MonoBehaviour
{
    enum TypeEntryItem { None, Entry, Upper, Lower }

    int N_COUNT_ITEM_FILE_BEFORE_DIRECTORY = 15;
    int N_COUNT_ITEM_FILE_INCLUDE_DIRECTORY = 15;
    int N_COUNT_ITEM_FILE_AFTER_DIRECTORY = 15;

    [SerializeField] Item prefabFile;
    [SerializeField] Item prefabDirectory;

    [SerializeField] RectTransform content;
    [SerializeField] ScrollRect scrollRect;
    [SerializeField] RectTransform viewPort;

    [SerializeField] PlaceAttachment placeAttachment;

    List<Item> list;

    Item directory;
    RectTransform rtDirectorView;
    RectTransform rtDirectoryItem;

    TypeEntryItem typeEntryItem = TypeEntryItem.None;

    private void Start()
    {
        Initialize();
    }

    void Initialize()
    {
        list = new List<Item>();
        CreateContent();
        scrollRect.onValueChanged.AddListener(scrollRectCallBack);
        CheckEntryDirectoryItemInViewPort();
    }

    private void scrollRectCallBack(Vector2 arg0)
    {
        CheckEntryDirectoryItemInViewPort();
    }

    Vector2 GetMinMaxY(RectTransform rectTransform)
    {
        Vector3[] corners = new Vector3[4];
        rectTransform.GetWorldCorners(corners);

        float yMin = corners.Min(v => v.y);
        float yMax = corners.Max(v => v.y);

        return new Vector2(yMin, yMax);
    }

    void CheckEntryDirectoryItemInViewPort() {
        Vector2 yMinMaxViewPort = GetMinMaxY(viewPort);
        Vector2 yMinMaxDirectoryItem = GetMinMaxY(rtDirectoryItem);

        float yMinViewPort = yMinMaxViewPort.x;
        float yMaxViewPort = yMinMaxViewPort.y;

        float yMinDirectoryItem = yMinMaxDirectoryItem.x;
        float yMaxDirectoryItem = yMinMaxDirectoryItem.y;

        bool isUpperScreen = yMaxDirectoryItem >= yMaxViewPort;
        bool isLowerScreen = yMinDirectoryItem <= yMinViewPort;

        if (isUpperScreen)
        {
            if (typeEntryItem != TypeEntryItem.Upper)
            {
                typeEntryItem = TypeEntryItem.Upper;
                placeAttachment.AttachToUp(rtDirectorView);
            }
        }
        else if (isLowerScreen)
        {
            if (typeEntryItem != TypeEntryItem.Lower)
            {
                typeEntryItem = TypeEntryItem.Lower;
                placeAttachment.AttachToDown(rtDirectorView);
            }
        }
        else
        {
            if (typeEntryItem != TypeEntryItem.Entry)
            {
                typeEntryItem = TypeEntryItem.Entry;
                directory.Attach(rtDirectorView);
            }
        }
        //Debug.LogFormat("typeEntryItem {0} isUpperScreen {1} isLowerScreen {2}", typeEntryItem, isUpperScreen, isLowerScreen);
    }

    void CreateContent()
    {
        CreateFilesUpper();
        CreateDirectoryWithFiles();
        CreateFilesLower();
    }

    void CreateFilesUpper()
    {
        CreateItems(prefabFile, N_COUNT_ITEM_FILE_BEFORE_DIRECTORY, content);
    }
    void CreateDirectoryWithFiles()
    {
        Item directory = CreateItem(prefabDirectory, content);
        this.directory = directory;
        rtDirectoryItem = directory.GetRectTransformItem();
        rtDirectorView = directory.GetRectTransformView();
        Transform transformContent = directory.GetTransformContent();

        CreateItems(prefabFile, N_COUNT_ITEM_FILE_INCLUDE_DIRECTORY, transformContent);
    }
    void CreateFilesLower()
    {

        CreateItems(prefabFile, N_COUNT_ITEM_FILE_AFTER_DIRECTORY, content);
    }

    void CreateItems(Item item, int count, Transform parent)
    {
        for (int i = 0; i < count; i++)
        {
            CreateItem(item, parent);
        }
    }

    Item CreateItem(Item _item, Transform parent)
    {
        Item item = Instantiate(_item);
        list.Add(item);

        item.SetParentCustom(parent);

        return item;
    }


    void Clear() {
        list.DestroyGameObjects();
    }

}
