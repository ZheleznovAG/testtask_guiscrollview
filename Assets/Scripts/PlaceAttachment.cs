﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceAttachment : MonoBehaviour
{
    [SerializeField] RectTransform upPlace;
    [SerializeField] RectTransform downPlace;

    public void AttachToUp(RectTransform rc) {
        rc.SetParent(upPlace, false);
    }

    public void AttachToDown(RectTransform rc)
    {
        rc.SetParent(downPlace, false);
    }

}
