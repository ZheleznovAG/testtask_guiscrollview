﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    public static void SetParentCustom(this Transform source, Transform to)
    {
        source.SetParent(to);
        source.localScale = Vector3.one;
    }
    public static void SetParentCustom<T>(this T source, Transform to) where T : Component
    {
        source.transform.SetParent(to);
        source.transform.localPosition = Vector3.zero;
        source.transform.localScale = Vector3.one;
    }
    public static void DestroyGameObjects<T>(this List<T> list) where T : Component
    {
        while (list.Count > 0)
        {
            int index = list.Count - 1;
            T item = list[index];

            if (item != null)
            {
                GameObject.Destroy(item.gameObject);
            }

            list.RemoveAt(index);
        }
    }
}
